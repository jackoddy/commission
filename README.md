# Commission Calculator

This project works out the commission earnt from `./data/sales`, organised by week. It outputs an object where the keys are the firs day of the week and the value is the commission earnt according to the rules defined in `./data/discountCodeRules`. Rules are orgnaised into a JSON to simulate the definitions coming from a database, as such it is easily extendable.

## About the Implementation

The key to working out the commission for each discount code lies in maintaining state somewhere in the application to ensure the correct sum is found for each subsequent discountCode of the same value. In order to count each discountCode the array would have to be either mapped, iterated over, or filtered. For this reason it seemed prudent to simply put the logic inside the iteration process instead of adding another step after counting instances.

The discountCode functions are ES6 generators. This implementation was chosen as a simple way to encapsulate the state (respective discountCode count) in a way that is inseperable from the logic itself. It is as simple as calling `.next()` on the correct discountCode generator to increment the count and get the correct value back, as defined by the rules.

The correct rules are added into the generic generator function before being exported in an object with keys of the same name. Therefore `generators[sale.discount_code]` will return the correct generator in order to be incremented.

### Running the Project

 - clone this repo
 - run `$ npm install`
 - run `$ npm start`
 
### Running the Tests

 - run `$ npm test`
