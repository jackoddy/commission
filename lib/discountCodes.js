import discountCodeRules from '../data/discountCodeRules'

function* discountCode (rules) {
    let count = 0
    let commission

    while(true){
        commission = rules[count] || commission
        count++
        yield commission
    }
}

export const initializeCodeGenerators = (codeRules) =>
    Object
        .keys(codeRules)
        .reduce((acc, key) => ({
            ...acc,
            [key]: discountCode(codeRules[key])
        }), {})

export default initializeCodeGenerators(discountCodeRules)
