import moment from 'moment'

export const sortSalesBy = unit => sales =>
    sales.reduce(
        (acc, sale) => {
            const saleWeek = moment(sale.date)
                  .startOf(unit)
                  .format("DD-MM-YYYY")

            const existing = acc[saleWeek]

            return {
                ...acc,
                [saleWeek]: existing ? [...existing, sale] : [sale]
            }
        },{})
