import discountCodes from './discountCodes'

const calculateCommission = ([sale, ...sales], commission = 0) => {
    if (!sale) return commission;

    const {value} = discountCodes[sale.discount_code].next()

    return calculateCommission(sales, commission + value)
};

export default calculateCommission
