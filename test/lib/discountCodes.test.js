import {initializeCodeGenerators} from '../../lib/discountCodes'

const mockSales = [
    {"date": "2018-08-02", "discount_code": "discount20"},
    {"date": "2018-08-15", "discount_code": "discount30"},
    {"date": "2018-08-15", "discount_code": "discount30"},
    {"date": "2018-08-15", "discount_code": "discount30"},
]

const mockRules = {
    "discount20": {
        "0": 50,
        "2": 55,
    },
    "discount30": {
        "0": 60,
        "2": 70,
    }
}

describe('discountCodes*', () => {


    it('works as a generator', () => {
        const discountCode = initializeCodeGenerators(mockRules).discount20
        expect(discountCode.next()).toEqual({value: 50, done: false})
    })

    it('returns the correct value on first call', () => {
        const discountCode = initializeCodeGenerators(mockRules).discount20
        expect(discountCode.next().value).toEqual(50)
    })

    it('returns the correct value on next tier', () => {
        const discountCode = initializeCodeGenerators(mockRules).discount30

        for(let n = 0; n < 2; n++){discountCode.next()}

        expect(discountCode.next().value).toEqual(70)
    })

    it('the value of the last tier sticks', () => {
        const discountCode = initializeCodeGenerators(mockRules).discount30

        for(let n = 0; n < 5; n++){discountCode.next()}

        expect(discountCode.next().value).toEqual(70)
    })
})

