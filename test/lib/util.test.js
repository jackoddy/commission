import {sortSalesBy} from '../../lib/util'
import moment from 'moment'

jest.mock('moment', () => jest.fn().mockImplementation(() => ({
    startOf: jest.fn().mockReturnThis(),
    format: jest.fn().mockReturnValueOnce('testDate1').mockReturnValue('testDate2')
})))

describe('util', () => {

    describe('sortBy()', () => {
        describe('getting the week', () => {
            beforeEach(jest.clearAllMocks)
            it('should pass date into moment\'s default function', () => {
              sortSalesBy('')([{date: 'testDate'}])
              expect(moment).toHaveBeenCalledWith('testDate')
            })

            it('should pass unit into moment\'s startOf function', () => {
                sortSalesBy('testUnit')([{date: ''}])
                const startOfMock = moment.mock.results[0].value.startOf
                expect(startOfMock).toHaveBeenCalledWith('testUnit')
            })

            it('should pass correct format string to moment', () => {
                sortSalesBy('')([{date: ''}])
                const formatMock = moment.mock.results[0].value.format
                expect(formatMock).toHaveBeenCalledWith('DD-MM-YYYY')
            })
        })
        describe('returning sales by week', () => {
            beforeEach(jest.clearAllMocks)
            it('should have keys that are the result of moment call', () => {
                const sale = {date: '', key1: 'value1'}
                const sales = sortSalesBy('')([sale])
                expect(sales.testDate1).toEqual([sale])
            })
            it('should have should group sales with same result from moment', () => {
                const day1= [
                    {date: '', key1: 'value1'},
                    {date: '', key1: 'value2'},
                ]

                const sales = sortSalesBy('')(day1)
                expect(sales.testDate1).toEqual(day1)
            })
        })
    })
})
