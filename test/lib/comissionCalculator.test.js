import calculateCommission from '../../lib/comissionCalculator'
import discountCodes from '../../lib/discountCodes'

const mockDiscount20 = jest.fn(() => ({value:1}))
const mockDiscount30 = jest.fn(() => ({value:1}))

jest.spyOn(discountCodes.discount20, 'next').mockImplementation(mockDiscount20)
jest.spyOn(discountCodes.discount30, 'next').mockImplementation(mockDiscount30)


const mockSales = [
    {"date": "2018-08-02", "discount_code": "discount20"},
    {"date": "2018-08-15", "discount_code": "discount30"},
    {"date": "2018-08-15", "discount_code": "discount30"},
]

describe('comissionCalculator()', () => {

    beforeEach(() => {
        jest.clearAllMocks()
    })

    describe('no sales', () => {
        const commission = calculateCommission([])

        it('should return 0 when passed an empty array', () => {
            expect(commission).toEqual(0)
        })

        it('should not call next() on discountCodes20', () => {
            expect(mockDiscount20.mock.calls.length).toEqual(0)
        })

        it('should not call next() on discountCodes30', () => {
            expect(mockDiscount30.mock.calls.length).toEqual(0)
        })
    })

    describe('with sales', () => {

        it('should call discount codes equal to number of sales', () => {
            const commission = calculateCommission(mockSales)
            expect(mockDiscount20.mock.calls.length +
                   mockDiscount30.mock.calls.length).toEqual(mockSales.length)
        })

        it('should call discount20 codes equal to number of sales', () => {
            const commission = calculateCommission(mockSales)
            expect(mockDiscount20.mock.calls.length).toEqual(1)
        })

        it('should call discount30 codes equal to number of sales', () => {
            const commission = calculateCommission(mockSales)
            expect(mockDiscount30.mock.calls.length).toEqual(2)
        })

        it('should return correct commission that is a sum of the result of next() calls', () => {
            const commission = calculateCommission(mockSales)
            expect(commission).toEqual(3)
        })
    })
})
