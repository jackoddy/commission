import calculateCommission from './lib/comissionCalculator'
import rawSales from './data/sales'
import { sortSalesBy } from './lib/util'

const sortedSales = sortSalesBy('isoWeek')(rawSales)

const generateCommissionReport = sales =>
    Object
      .keys(sales)
      .reduce((acc, period) => ({
          ...acc,
          [period]: calculateCommission(sales[period])
      }), {})

console.info(generateCommissionReport(sortedSales))
